# CHANGELOG

## 4.0.0 - 2025-02-27

### Fixed

- #42 #37 Revert !39 (Les methodes `send()` des classes dérivées de `FacteurApi` renvoient systématiquement un booléen)

### Changed

- Compatible SPIP 4.2 minimum
- Chaînes de langue au format SPIP 4.1+

## 3.4.2 - 2025-02-24

### Fixed

- #36 mettre le total des envois terminés ou achevés dans la bonne colonne
